# uniapp-DEMO

#### 项目介绍
空余时间，用Dcloud新推出的uni-app重构公司app的一个demo

#### 软件架构
软件架构说明


#### 安装教程

1. 下载HBuild-X
2. 建立一个项目文件，导入项目
3. 点击小程序IDE运行或者手机运行

#### 使用说明

1. 这是一个基于uni-app的项目，所以要用HBuild-X 来打开预览(支持小程序、安卓、苹果)

#### 参与贡献

1.undefined


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)